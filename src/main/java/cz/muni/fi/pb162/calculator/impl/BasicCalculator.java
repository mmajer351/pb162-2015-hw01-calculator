package cz.muni.fi.pb162.calculator.impl;

import cz.muni.fi.pb162.calculator.Calculator;
import cz.muni.fi.pb162.calculator.Result;

/**
 * @author Matej Majer
 */
public class BasicCalculator implements Calculator {

    /**
     * Evaluate textual input and perform computation
     *
     * @param input input string
     * @return result
     */
    @Override
    public Result eval(String input) {
        String[] args = input.split(" ");
        if (args.length > 3) {
            return new CalculationResult(WRONG_ARGUMENTS_ERROR_MSG);
        }
        switch (args[0]) {
            case SUM_CMD :
                return sum(Double.parseDouble(args[1]), Double.parseDouble(args[2]));
            case SUB_CMD :
                return sub(Double.parseDouble(args[1]), Double.parseDouble(args[2]));
            case MUL_CMD :
                return mul(Double.parseDouble(args[1]), Double.parseDouble(args[2]));
            case DIV_CMD :
                return div(Double.parseDouble(args[1]), Double.parseDouble(args[2]));
            case FAC_CMD :
                if (args.length != 2) {
                    return new CalculationResult(WRONG_ARGUMENTS_ERROR_MSG);
                }
                return fac(Integer.parseInt(args[1]));
            default:  return new CalculationResult(UNKNOWN_OPERATION_ERROR_MSG);
        }
    }

    /**
     * Computes the sum of two numbers (x + y)
     *
     * @param x first number
     * @param y second number
     * @return result
     */
    @Override
    public Result sum(double x, double y) {
        return new CalculationResult(x+y);
    }

    /**
     * Subtract two numbers (x - y)
     *
     * @param x first number
     * @param y second number
     * @return result
     */
    @Override
    public Result sub(double x, double y) {
        return new CalculationResult(x - y);
    }

    /**
     * Multiply two number (x * y(
     *
     * @param x first number
     * @param y second number
     * @return result
     */
    @Override
    public Result mul(double x, double y) {
        return new CalculationResult(x * y);
    }

    /**
     * Calculate the division of two numbers (x / y)
     *
     * @param x first number
     * @param y second number
     * @return result
     */
    @Override
    public Result div(double x, double y) {
        if (y == 0) {
            return new CalculationResult(COMPUTATION_ERROR_MSG);
        }
        return new CalculationResult(x / y);
    }

    /**
     * Computes the factorial of given number;
     * We guarantee that the result for all tested inputs will fit into double;
     *
     * @param x input number
     * @return result
     */
    @Override
    public Result fac(int x) {
        if (x < 0) {
            return new CalculationResult(COMPUTATION_ERROR_MSG);
        }
        int fact = 1;
        for (int i = 1; i <= x; i++) {
            fact *= i;
        }
        return new CalculationResult(fact);
    }
}
