package cz.muni.fi.pb162.calculator.impl;

import cz.muni.fi.pb162.calculator.Calculator;
import cz.muni.fi.pb162.calculator.NumeralConverter;
import cz.muni.fi.pb162.calculator.Result;

/**
 * @author Matej Majer
 */
public interface ConvertingCalculator extends NumeralConverter, Calculator {
    /**
     * Evaluate textual input and perform computation
     *
     * @param input input string
     * @return result
     */
    @Override
    Result eval(String input);

    /**
     * Computes the sum of two numbers (x + y)
     *
     * @param x first number
     * @param y second number
     * @return result
     */
    @Override
    Result sum(double x, double y);

    /**
     * Subtract two numbers (x - y)
     *
     * @param x first number
     * @param y second number
     * @return result
     */
    @Override
    Result sub(double x, double y);

    /**
     * Multiply two number (x * y(
     *
     * @param x first number
     * @param y second number
     * @return result
     */
    @Override
    Result mul(double x, double y);

    /**
     * Calculate the division of two numbers (x / y)
     *
     * @param x first number
     * @param y second number
     * @return result
     */
    @Override
    Result div(double x, double y);

    /**
     * Computes the factorial of given number;
     * We guarantee that the result for all tested inputs will fit into double;
     *
     * @param x input number
     * @return result
     */
    @Override
    Result fac(int x);

    /**
     * Convert a number in arbitrary numeral system (up to base 16) to decimal
     * <p>
     * Supported bases 2-16
     *
     * @param base   base of source numeral system (e.g. 2 for binary)
     * @param number number in source numeral system
     * @return result with numeric value set
     */
    @Override
    Result toDec(int base, String number);

    /**
     * Convert a number from decimal system to any other numeral system (up to base 16)
     * <p>
     * Supported bases 2-16
     *
     * @param base   base of target numeral system (e.g. 2 for binary)
     * @param number number in decimal numeral system
     * @return result with alphanumeric value set
     */
    @Override
    Result fromDec(int base, int number);
}
