package cz.muni.fi.pb162.calculator.impl;

import cz.muni.fi.pb162.calculator.Calculator;
import cz.muni.fi.pb162.calculator.Result;

/**
 * @author Matej Majer
 */
public class CalculationResult implements Result {
    private String result;
    private double numResult;

    /**
     * Constuctor for alphanumeric result
     * @param result alphanumeric result
     */
    public CalculationResult(String result) {
        this.result = result;
    }

    /**
     * Constructor for numeric result
     * @param numResult numeric result
     */
    public CalculationResult(double numResult) {
        this.numResult = numResult;
    }

    /**
     * Method used to determine whether result is successful
     *
     * @return true if this result contains value
     */
    @Override
    public boolean isSuccess() {
        if (!isNumeric() && !isAlphanumeric()) {
            return false;
        }
        if (result != null) {
            if (result.equals(Calculator.COMPUTATION_ERROR_MSG)) {
                return false;
            }
            if (result.equals(Calculator.WRONG_ARGUMENTS_ERROR_MSG)) {
                return false;
            }
            if (result.equals(Calculator.UNKNOWN_OPERATION_ERROR_MSG)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Method used to determine whether result contains alphanumeric value
     *
     * @return true if this result has alphanumeric value
     */
    @Override
    public boolean isAlphanumeric() {
        return (!(result == null));
    }

    /**
     * Method used to determine whether result contains numeric value
     *
     * @return true if this result has numeric value
     */
    @Override
    public boolean isNumeric() {
        return (!isAlphanumeric() && !Double.isNaN(numResult));
    }

    /**
     * If result contains numeric value, this method should be used to retrieve it
     *
     * @return value if successful and numeric,  Double.NaN otherwise;
     */
    @Override
    public double getNumericValue() {
        if(isNumeric() && isSuccess()) {
            return numResult;
        }
        return Double.NaN;
    }

    /**
     * If result contains alphanumeric value or error message, this method should be used to retrieve it
     *
     * @return value if successful and alphanumeric, error message if not successful, null otherwise;
     */
    @Override
    public String getAlphanumericValue() {
        if(isAlphanumeric()) {
            if(isSuccess()) {
                return result;
            }
            return result;
        }
        return null;
    }
}
