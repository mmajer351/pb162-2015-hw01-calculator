package cz.muni.fi.pb162.calculator.impl;

import cz.muni.fi.pb162.calculator.Result;

/**
 * @author Matej Majer
 */
public class AdvancedCalculator extends BasicCalculator implements ConvertingCalculator{

    /**
     * Evaluate textual input and perform computation
     *
     * @param input input string
     * @return result
     */
    @Override
    public Result eval(String input) {
        String[] args = input.split(" ");
        if (args.length != 3) {
            return super.eval(input);
        }

        if (!args[0].equals(TO_DEC_CMD) && !args[0].equals(FROM_DEC_CMD)) {
            return super.eval(input);
        }

        int arg1 = Integer.parseInt(args[1]);

        switch (args[0]) {
            case TO_DEC_CMD :
                return toDec(arg1, args[2]);
            case FROM_DEC_CMD :
                int arg2 = Integer.parseInt(args[2]);
                return fromDec(arg1, arg2);
            default: return super.eval(input);
        }
    }

    /**
     * Computes the sum of two numbers (x + y)
     *
     * @param x first number
     * @param y second number
     * @return result
     */
    @Override
    public Result sum(double x, double y) {
        return super.sum(x, y);
    }

    /**
     * Subtract two numbers (x - y)
     *
     * @param x first number
     * @param y second number
     * @return result
     */
    @Override
    public Result sub(double x, double y) {
        return super.sub(x, y);
    }

    /**
     * Multiply two number (x * y(
     *
     * @param x first number
     * @param y second number
     * @return result
     */
    @Override
    public Result mul(double x, double y) {
        return super.mul(x, y);
    }

    /**
     * Calculate the division of two numbers (x / y)
     *
     * @param x first number
     * @param y second number
     * @return result
     */
    @Override
    public Result div(double x, double y) {
        return super.div(x, y);
    }

    /**
     * Computes the factorial of given number;
     * We guarantee that the result for all tested inputs will fit into double;
     *
     * @param x input number
     * @return result
     */
    @Override
    public Result fac(int x) {
        return super.fac(x);
    }

    /**
     * Convert a number in arbitrary numeral system (up to base 16) to decimal
     * <p>
     * Supported bases 2-16
     *
     * @param base   base of source numeral system (e.g. 2 for binary)
     * @param number number in source numeral system
     * @return result with numeric value set
     */
    @Override
    public Result toDec(int base, String number) {
        if (base < 2 || base > 16) {
            return new CalculationResult(COMPUTATION_ERROR_MSG);
        }
        if (number.contains("#")) {
            return new CalculationResult(COMPUTATION_ERROR_MSG);
        }
        int count = 0;
        for (int i = 0; i < number.length(); i++) {
            char c = number.charAt(i);
            int d = DIGITS.indexOf(c);
            count = base * count + d;
        }
        return new CalculationResult(count);
    }

    /**
     * Convert a number from decimal system to any other numeral system (up to base 16)
     * <p>
     * Supported bases 2-16
     *
     * @param base   base of target numeral system (e.g. 2 for binary)
     * @param number number in decimal numeral system
     * @return result with alphanumeric value set
     */
    @Override
    public Result fromDec(int base, int number) {
        if (base < 2 || base > 16) {
            return new CalculationResult(COMPUTATION_ERROR_MSG);
        }
        if (number == 0) {
            return new CalculationResult("0");
        }
        String res = "";
        while (number > 0) {
            int digit = number % base;
            res = DIGITS.charAt(digit) + res;
            number = number / base;
        }
        return new CalculationResult(res);
    }
}
